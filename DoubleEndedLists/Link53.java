package DoubleEndedLists;

class Link53 {
    public long dData; // data item
    public Link53 next; // next link in list
    
    public Link53(long d) // constructor
    {
        dData = d;
    }

    public void displayLink() // display this link
    {
        System.out.print(dData + " ");
    }
    
} 