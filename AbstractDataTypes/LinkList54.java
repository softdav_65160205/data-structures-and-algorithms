package AbstractDataTypes;

public class LinkList54 {
    private Link54 first; // ref to first item on list
    // -------------------------------------------------------------

    public LinkList54() // constructor
    {
        first = null;
    } // no items on list yet
    // -------------------------------------------------------------

    public boolean isEmpty() // true if list is empty
    {
        return (first == null);
    }

    // -------------------------------------------------------------
    public void insertFirst(long dd) // insert at start of list
    { // make new link
        Link54 newLink = new Link54(dd);
        newLink.next = first; // newLink --> old first
        first = newLink; // first --> newLink
    }

    // -------------------------------------------------------------
    public long deleteFirst() // delete first item
    { // (assumes list not empty)
        Link54 temp = first; // save reference to link
        first = first.next; // delete it: first-->old next
        return temp.dData; // return deleted link
    }

    // -------------------------------------------------------------
    public void displayList() {
        Link54 current = first; // start at beginning of list
        while (current != null) // until end of list,
        {
            current.displayLink(); // print data
            current = current.next; // move to next link
        }
        System.out.println("");

    }
}
