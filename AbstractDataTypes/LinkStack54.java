package AbstractDataTypes;

public class LinkStack54 {
    private LinkList54 theList;

    // --------------------------------------------------------------
    public LinkStack54() // constructor
    {
        theList = new LinkList54();
    }

    // --------------------------------------------------------------
    public void push(long j) // put item on top of stack
    {
        theList.insertFirst(j);
    }

    // --------------------------------------------------------------
    public long pop() // take item from top of stack
    {
        return theList.deleteFirst();
    }

    // --------------------------------------------------------------
    public boolean isEmpty() // true if stack is empty
    {
        return (theList.isEmpty());
    }

    // --------------------------------------------------------------
    public void displayStack() {
        System.out.print("Stack (top-->bottom): ");
        theList.displayList();
    }
}
