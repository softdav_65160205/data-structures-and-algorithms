package AbstractDataTypes;

public class Link54 {
    public long dData; // data item
    public Link54 next; // next link in list

    // -------------------------------------------------------------
    public Link54(long dd) // constructor
    {
        dData = dd;
    }

    // -------------------------------------------------------------
    public void displayLink() // display ourself
    {
        System.out.print(dData + " ");
    }
}
