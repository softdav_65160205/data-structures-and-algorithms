import java.util.Scanner;
public class ArrayDeletionsLab {
    public static int[] deleteElementByIndex(int[] Arr, int index) {
        int[] newArr = new int[Arr.length - 1];
        int deleteindex = index;
        for (int i = 0; i < deleteindex; i++) {
            newArr[i] = Arr[i];
        }

        for (int j = deleteindex + 1; j < Arr.length; j++) {
            newArr[j - 1] = Arr[j];
        }
        int[] Arr1 = newArr;
        return Arr1;
    }

    public static void deleteElementByValue(int[] Arr, int value) {
        int count = 0;
        int newIndex = 0;
        for (int num : Arr) {
            if (num == value) {
                count++;
            }
        }

        int[] newArr = new int[Arr.length - count];

        for (int i = 0; i < Arr.length; i++) {
            if (Arr[i] != value) {
                newArr[newIndex] = Arr[i];
                newIndex++;
            }
        }

        for (int i = 0; i < newArr.length; i++) {
            System.out.print(newArr[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) throws Exception {
        int[] Arr = { 1, 2, 3, 4, 5 };
        Scanner kb = new Scanner(System.in);
        System.out.println("Array after deleting element at index :");
        int index = kb.nextInt();
        deleteElementByIndex(Arr, index);
        int[] deleteIndex = deleteElementByIndex(Arr, index);
        for (int i = 0; i < deleteIndex.length; i++) {
            System.out.print(deleteIndex[i] + " ");
        }
        System.out.println();

        System.out.println("Array after deleting element with value :");
        int value = kb.nextInt();
        deleteElementByValue(deleteElementByIndex(Arr, index), value);
    }
}