import java.util.Scanner;

public class RemoveElement {
    public static int countnotVal(int[] nums,int val){
        int k=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i] != val){
                k++;
            }
        }
        return k;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int range = sc.nextInt();
        int[] nums = new int[range];
        
        for(int i=0;i<range;i++){
            nums[i] = sc.nextInt();
        }

        int val = sc.nextInt();
        int k = countnotVal(nums,val);
        System.out.println("k = "+ k);
        System.out.print("nums = [" );

        for(int i=0;i<nums.length;i++){
            if(nums[i] != val){
                System.out.print(nums[i]+",");
            }
        }

        for(int i=0;i<nums.length;i++){
            if(nums[i] == val){
                System.out.print("_"+" ");
            }
        }System.out.print("]" );
    }

}
