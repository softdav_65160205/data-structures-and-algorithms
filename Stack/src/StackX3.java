public class StackX3 {
    private int maxSize;
    private char[] stackArray;
    private int top;

    // ----------------------------------------------------------//
    public StackX3(int s) {
        maxSize = s;
        stackArray = new char[maxSize];
        top -= 1;
    }

    // ----------------------------------------------------------//
    public void push(char j) {
        stackArray[++top] = j;
    }

    // ----------------------------------------------------------//
    public char pop() // take item from top of stack
    {
        return stackArray[top--];
    }

    // --------------------------------------------------------------
    public char peek() // peek at top of stack
    {
        return stackArray[top];
    }

    // --------------------------------------------------------------
    public boolean isEmpty() // true if stack is empty
    {
        return (top == -1);
    }
    // --------------------------------------------------------------
} // end class StackX
  ////////////////////////////////////////////////////////////////
