package LinkedList;

public class LinkListApp51 {
    public static void main(String[] args) {
        LinkList51 theList = new LinkList51();

        theList.insertFirst(22, 2.99); // insert four items
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);

        theList.displayList();
        while (!theList.isEmpty()) {
            Link51 aLink = theList.deleteFirst();
            System.out.println("Delete ");
            aLink.displayLink();
            System.out.println("");
        }
        theList.displayList();
    }
}
