package LinkedList;

public class Link51 {
    
    public int iData;
    public double dData;
    public Link51 next;

    //////////////////////////////////////////////////////////////
    public Link51(int id, double dd) {
        iData = id;
        dData = dd;
    }

    //////////////////////////////////////////////////////////////
    public void displayLink() {
        System.out.println("{" + iData + ", " + dData + "} ");
    }
}
