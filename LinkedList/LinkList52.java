package LinkedList;

public class LinkList52 {
    private Link52 first; 

    public LinkList52() {
        first = null; 
    }

    public void insertFirst(int id, double dd) { 
        Link52 newLink = new Link52(id, dd);
        newLink.next = first; 
        first = newLink; 
    }

    public Link52 find(int key) { 
        Link52 current = first; 
        while (current.iData != key) {
            if (current.next == null) 
                return null; 
            else 
                current = current.next; 
        }
        return current;
    }

    public Link52 delete(int key) { 
        Link52 current = first; 
        Link52 previous = first;
        while (current.iData != key) {
            if (current.next == null)
                return null; 
            else {
                previous = current;
                current = current.next;
            }
        } 
        if (current == first) 
            first = first.next; 
        else 
            previous.next = current.next;
        return current;
    }

    public void displayList() {
        System.out.print("List (first-->last): ");
        Link52 current = first; 
        while (current != null) {
            current.displayLink(); 
            current = current.next; 
        }
        System.out.println("");
    }
}
