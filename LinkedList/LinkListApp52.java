package LinkedList;

public class LinkListApp52 {
    public static void main(String[] args) {
        LinkList52 theList = new LinkList52();

        theList.insertFirst(22, 2.99); // insert 4 items
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);

        theList.displayList(); // display list
        Link52 f = theList.find(44);

        if (f != null) {
            System.out.println("Found link with key " + f.iData);
        } else {
            System.out.println("Can’t find link");
        }

        Link52 d = theList.delete(66); // delete item
        if (d != null) {
            System.out.println("Deleted link with key " + d.iData);
        } else {
            System.out.println("Can’t delete link");
        }

        theList.displayList();
    }
}
