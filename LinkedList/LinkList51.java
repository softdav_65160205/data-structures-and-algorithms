package LinkedList;

public class LinkList51 {
    private Link51 first;

    //////////////////////////////////////////////
    public void LinkList() {
        first = null;
    }

    /////////////////////////////////////////////
    public boolean isEmpty() {
        return (first == null);
    }

    /////////////////////////////////////////////
    public void insertFirst(int id, double dd) {
        Link51 newLink = new Link51(id, dd);
        newLink.next = first;
        first = newLink;
    }

    /////////////////////////////////////////////
    public Link51 deleteFirst() {
        Link51 temp = first;
        first = first.next;
        return temp;
    }

    public void displayList() {
        System.out.println("List(first-->last): ");
        Link51 current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println("");
    }
}