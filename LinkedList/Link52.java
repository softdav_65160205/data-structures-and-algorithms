package LinkedList;

public class Link52 {

    public int iData; // data item (key)
    public double dData; // data item
    public Link52 next; // next link in list
    // -------------------------------------------------------------

    public Link52(int id, double dd) {
        iData = id;
        dData = dd;
    }

    // -------------------------------------------------------------
    public void displayLink() {
        System.out.println("{" + iData + ", " + dData + "} ");
    }
    // end class Link
}
