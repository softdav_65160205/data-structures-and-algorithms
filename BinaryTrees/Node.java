class Node {
    person p1; // reference to person object
    Node leftChild; // this node’s left child
    Node rightChild; // this node’s right child
}